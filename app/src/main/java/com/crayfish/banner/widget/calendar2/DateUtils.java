package com.crayfish.banner.widget.calendar2;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.util.Log;

/**
 * Created by crayfish on 2016/8/22.
 */
public class DateUtils {

    /**
     * 通过年份和月份 得到当月的日子
     *
     * @param year
     * @param month
     * @return
     */
    public static int getMonthDays(int year, int month) {
        month++;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
                    return 29;
                }else{
                    return 28;
                }
            default:
                return  -1;
        }
    }
    /**
     * 返回当前月份1号位于周几
     * @param year
     * 		年份
     * @param month
     * 		月份，传入系统获取的，不需要正常的
     * @return
     * 	日：1		一：2		二：3		三：4		四：5		五：6		六：7
     */
    public static int getFirstDayWeek(int year, int month){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        Log.d("DateView", "DateView:First:" + calendar.getFirstDayOfWeek());
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static int[] getWeekDays(int year, int month,int day){
        int[] weekDays = new int[7];
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        for (int i=0;i<7;i++){
            int mDay = day+(1-week+i);
            if(mDay>0 && mDay<=getMonthDays(year,month)){
                weekDays[i] = mDay;
            }else{
                weekDays[i] = 0;
            }
        }
        return weekDays;
    }

    /**
     * 返回一个月有几周
     * @param year
     * @param month
     * @return
     */
    public static int getMonthOfWeek(int year,int month){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        Log.d("DateView", "DateView:First:" + calendar.getFirstDayOfWeek());
        return calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
    }

    public static boolean isEqualDay(int year, int month,int day,String tagDay){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day,0,0,0);
        String currDay = sdf.format(calendar.getTime());
        return currDay.equals(tagDay);
    }
}
