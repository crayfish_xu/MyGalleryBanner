package com.crayfish.banner.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crayfish.banner.R;

/**
 * 组合View
 * Created by crayfish on 2016/8/9.
 */
public class CompositeView extends RelativeLayout{

    private String mTitleName;
    private float mTitleTextSize;
    private int mTitleTextColor;

    private String mLeftText;
    private float mLeftTextSize;
    private int mLeftTextColor;
    private Drawable mLeftBackground;
    private String mRightText;
    private float mRightTextSize;
    private int mRightTextColor;
    private Drawable mRightBackground;

    private Button mLeftButton;
    private Button mRightButton;
    private TextView mTitleView;



    public CompositeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public CompositeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CompositeView);
        mTitleName = ta.getString(R.styleable.CompositeView_titleName);
        mTitleTextSize = ta.getDimension(R.styleable.CompositeView_titleTextSize,10.0f);
        mTitleTextColor = ta.getColor(R.styleable.CompositeView_mTitleTextColor,0);

        mLeftText = ta.getString(R.styleable.CompositeView_leftText);
        mLeftTextSize = ta.getDimension(R.styleable.CompositeView_leftTextSize,10.0f);
        mLeftTextColor = ta.getColor(R.styleable.CompositeView_leftTextColor,0);
        mLeftBackground = ta.getDrawable(R.styleable.CompositeView_leftBackground);

        mRightText = ta.getString(R.styleable.CompositeView_rightText);
        mRightTextSize = ta.getDimension(R.styleable.CompositeView_rightTextSize,10.0f);
        mRightTextColor = ta.getColor(R.styleable.CompositeView_rightTextColor,0);
        mRightBackground = ta.getDrawable(R.styleable.CompositeView_rightBackground);

        ta.recycle();
        initView(context);
    }

    /**
     * 初始化View
     */
    private void initView(Context context){
        mLeftButton = new Button(context);
        mRightButton = new Button(context);
        mTitleView = new TextView(context);

        mLeftButton.setText(mLeftText);
        mLeftButton.setTextSize(mLeftTextSize);
        mLeftButton.setTextColor(mLeftTextColor);
        mLeftButton.setBackground(mLeftBackground);

        mRightButton.setText(mRightText);
        mRightButton.setTextSize(mRightTextSize);
        mRightButton.setTextColor(mRightTextColor);
        mRightButton.setBackground(mRightBackground);

        mTitleView.setText(mTitleName);
        mTitleView.setTextSize(mTitleTextSize);
        mTitleView.setTextColor(mTitleTextColor);
        mTitleView.setGravity(Gravity.CENTER);

        LayoutParams mLeftParams = new LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mLeftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        addView(mLeftButton,mLeftParams);

        LayoutParams mRightParams = new LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mRightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        addView(mRightButton,mRightParams);

        LayoutParams mTitleParams = new LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mTitleParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        addView(mTitleView,mTitleParams);

        mLeftButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.leftClick();
            }
        });
        mRightButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.rightClick();
            }
        });

    }

    public topBarClickListener listener;

    public void setOnTopBarClickListener(topBarClickListener listener){
        this.listener = listener;
    }

    public interface topBarClickListener{
        void leftClick();
        void rightClick();
    }
}
