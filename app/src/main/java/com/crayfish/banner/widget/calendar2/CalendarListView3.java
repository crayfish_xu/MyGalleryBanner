package com.crayfish.banner.widget.calendar2;

import android.content.Context;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.crayfish.banner.R;

/**
 * Created by lin on 2016/9/26.
 */
public class CalendarListView3 extends LinearLayout{

//    private ListView listView;
//    private CalendarView calendarView;
//    private WeekDayView weekDayView;

    private ViewDragHelper mDragger;
    private ViewDragHelper.Callback callback;


    @Override
    protected void onFinishInflate() {
//        calendarView = (CalendarView)this.findViewById(R.id.calendarView);
//        weekDayView = (WeekDayView)this.findViewById(R.id.weekDayView);
//        listView = (ListView)this.findViewById(R.id.listView);
        super.onFinishInflate();
    }

    public CalendarListView3(Context context, AttributeSet attrs) {
        super(context, attrs);
//        LayoutInflater.from(context).inflate(R.layout.layout_calendar_listview,this);
        callback = new DraggerCallBack();
        mDragger = ViewDragHelper.create(this,1.0f,callback);
    }

    class DraggerCallBack extends ViewDragHelper.Callback {
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return true;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
//            int topWidth = -(calendarView.getHeight()-weekDayView.getHeight());
//            int bootomWidth = calendarView.getHeight();
            return top;
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            //松手的时候 判断如果是这个view 就让他回到起始位置
//            if (releasedChild == calendarView) {
//                //这边代码你跟进去去看会发现最终调用的是startScroll这个方法 所以我们就明白还要在computeScroll方法里刷新
//                mDragger.settleCapturedViewAt(0, 200);
//                invalidate();
//            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDragger.shouldInterceptTouchEvent(ev);
    }
    @Override
    public void computeScroll() {
        if (mDragger.continueSettling(true)) {
            invalidate();
            }
        }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            mDragger.processTouchEvent(event);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public ListAdapter adapter;

    public void setAdapter(ArrayAdapter adapter) {
        this.adapter = adapter;
//        listView.setAdapter(adapter);
    }

}
