package com.crayfish.banner.widget;

import android.content.Context;
import android.support.v4.graphics.drawable.DrawableWrapper;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.crayfish.banner.R;

/**
 * Created by lin on 2016/8/5.
 */
public class GalleryBannerView extends FrameLayout{
    public GalleryBannerView(Context context) {
        super(context);
        init(context);
    }

    public GalleryBannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GalleryBannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutParams fparams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fparams.gravity = Gravity.CENTER;
        this.setLayoutParams(fparams);
        LayoutParams layoutParams = new LayoutParams(200,100);
        layoutParams.setMargins(100,0,0,0);
        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        LayoutParams params1 = new LayoutParams(200,60);
        params1.setMargins(200,0,0,0);
        params1.gravity = Gravity.CENTER_VERTICAL;
        LayoutParams params2 = new LayoutParams(200,60);
        params2.gravity = Gravity.CENTER_VERTICAL;
        ImageView iv1 = new ImageView(context);
        iv1.setImageResource(R.drawable.aaa);
        iv1.setAlpha(0.8f);
        iv1.setLayoutParams(params2);
        ImageView iv2 = new ImageView(context);
        iv2.setImageResource(R.drawable.aaa);
        iv2.setAlpha(0.8f);
        iv2.setLayoutParams(params1);
        ImageView iv3 = new ImageView(context);
        iv3.setImageResource(R.drawable.aaa);
        iv3.setLayoutParams(layoutParams);
        addView(iv1);
        addView(iv2);
        addView(iv3);
    }
}
