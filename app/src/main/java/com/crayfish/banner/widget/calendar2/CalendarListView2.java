package com.crayfish.banner.widget.calendar2;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Scroller;

import com.crayfish.banner.R;

/**
 * Created by lin on 2016/8/31.
 */
public class CalendarListView2 extends FrameLayout{

    private LinearLayout linearLayout;
    private ListView listView;
    private CalendarView calendarView;
    private WeekDayView weekDayView;

    private ListAdapter adapter;
    private Scroller scroller;
    private Status status = Status.IDLE;
    CalendarView.SelectedRowColumn selectedRowColumn;

    public CalendarListView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        scroller = new Scroller(context);
        LayoutInflater.from(context).inflate(R.layout.layout_calendar_listview2,this);
        linearLayout = (LinearLayout)this.findViewById(R.id.ll);
        calendarView = (CalendarView)this.findViewById(R.id.calendarView);
        weekDayView = (WeekDayView)this.findViewById(R.id.weekDayView);
        listView = (ListView)this.findViewById(R.id.listView);
        calendarView.setOnDateClick(new CalendarView.DateClick() {
            @Override
            public void onDateClick(int year, int month, int day) {
                weekDayView.setDay(year, month, day);
            }
        });
        weekDayView.setOnDateClick(new WeekDayView.DateClick() {
            @Override
            public void onDateClick(int year, int month, int day) {
                calendarView.setDay(year, month, day);
            }
        });
    }

    private int getListViewScrollY() {
        View c = listView.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int top = c.getTop();
        return -top + firstVisiblePosition * c.getHeight() ;
    }

    private int downY,startY;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        selectedRowColumn = calendarView.getSelectedRowColumn();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downY = (int)ev.getRawY();
                startY = (int)ev.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                int currY = (int)ev.getRawY();

                Log.d("DateView","ScrollY:"+linearLayout.getScrollY());
                if(listView.getFirstVisiblePosition() !=0||getListViewScrollY()>0||status == Status.LIST){
                    Log.d("DateView","ListScrollY:"+getListViewScrollY());
                    status = Status.LIST;
                    return super.dispatchTouchEvent(ev);
                }
                if(linearLayout.getScrollY() >= calendarView.getHeight()-weekDayView.getHeight()&&
                        currY < startY){//上推
                    Log.d("DateView","ListScrollY:"+status.toString());
                    if(status == Status.IDLE){
                        return super.dispatchTouchEvent(ev);
                    }else {
                        linearLayout.scrollTo(0, calendarView.getHeight() - weekDayView.getHeight());
                        startY = currY;
                        return true;
                    }
                }
                if(linearLayout.getScrollY() <= 0&& currY > startY){//下拉
                        linearLayout.scrollTo(0,0);
                        startY = currY;
                        return true;
                }
                status = Status.DRAGGING1;
                if(linearLayout.getScrollY() >= ((selectedRowColumn.row) *
                        weekDayView.getHeight())&&currY < startY){
                    post(new Runnable() {
                        @Override
                        public void run() {
                            weekDayView.setVisibility(VISIBLE);
                            status = Status.DRAGGING2;
                        }
                    });
                }
                if(linearLayout.getScrollY() <= ((selectedRowColumn.row) *
                        weekDayView.getHeight())&&currY > startY){
                    post(new Runnable() {
                        @Override
                        public void run() {
                            weekDayView.setVisibility(GONE);
                            status = Status.DRAGGING1;
                        }
                    });
                }
                final int dy = startY - currY;
                linearLayout.scrollBy(0,dy);
                post(new Runnable() {
                    @Override
                    public void run() {
                        int listViewHeight = CalendarListView2.this.getHeight();
                        if(CalendarListView2.this.getHeight()<(calendarView.getHeight()-weekDayView.getHeight())){
                            listViewHeight = CalendarListView2.this.getHeight() + dy;
                        }
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) listView.getLayoutParams();
                        layoutParams.height = listViewHeight;
                        listView.setLayoutParams(layoutParams);
                    }
                });
                startY = currY;
                return true;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                currY = (int)ev.getRawY();
                if (status != Status.DRAGGING1&&status!=Status.DRAGGING2) {
                    status = Status.IDLE;
                    return super.dispatchTouchEvent(ev);
                }
                if(currY - downY > 10) {//下拉
                    if(status == Status.DRAGGING1) {
                        scroller.startScroll(0, linearLayout.getScrollY(), 0, -(linearLayout.getScrollY()-(selectedRowColumn.row) * weekDayView.getHeight()));
                        status = Status.DRAGGING2;
                    }else{
                        scroller.startScroll(0, linearLayout.getScrollY(), 0, -(calendarView.getHeight()-weekDayView.getHeight()-
                                linearLayout.getScrollY()));
                        status = Status.IDLE;
                    }
                    invalidate();
                }
                if(currY - downY < -10){//上推
                    if(status == Status.DRAGGING1) {
                        scroller.startScroll(0, linearLayout.getScrollY(), 0,
                                ((selectedRowColumn.row) * weekDayView.getHeight() - linearLayout.getScrollY()));
                    }else{
                        scroller.startScroll(0, linearLayout.getScrollY(), 0,
                                (calendarView.getHeight()-weekDayView.getHeight() - linearLayout.getScrollY()));
                        status = Status.IDLE;
                    }
                    invalidate();
                }

                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        //判断Scroller是否完成
        if(scroller.computeScrollOffset()){
            Log.d("DateView","CurrY:"+scroller.getCurrY());
            int currY = scroller.getCurrY();
            if(currY<=0){
                currY =0;
            }
            linearLayout.scrollTo(0,currY);
            if(scroller.getCurrY()>=(selectedRowColumn.row) * weekDayView.getHeight()&&status == Status.DRAGGING1){
                post(new Runnable() {
                    @Override
                    public void run() {
                        weekDayView.setVisibility(VISIBLE);
                    }
                });
                Log.d("DateView","one");
                scroller.startScroll(0, linearLayout.getScrollY(), 0,
                        (calendarView.getHeight()-weekDayView.getHeight() - linearLayout.getScrollY()));
                status = Status.IDLE;
            }
            if(scroller.getCurrY()<=(selectedRowColumn.row) * weekDayView.getHeight()&&status == Status.DRAGGING2){
                post(new Runnable() {
                    @Override
                    public void run() {
                        weekDayView.setVisibility(GONE);
                    }
                });
                Log.d("DateView","two");
                scroller.startScroll(0, linearLayout.getScrollY(), 0,-(linearLayout.getScrollY()));
                status = Status.IDLE;
            }
            //重绘触发computeScroll
            postInvalidate();
        }
    }

    public void setAdapter(ListAdapter adapter){
        this.adapter = adapter;
        listView.setAdapter(adapter);
    }

    public enum Status{
        IDLE,//常态
        DRAGGING1,//拖动状态1
        DRAGGING2,//拖动状态2
        LIST
    }
}
