package com.crayfish.banner.widget.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by lin on 2016/8/17.
 */
public class GoodCalendar extends GridView{
    public GoodCalendar(Context context) {
        super(context);
    }

    public GoodCalendar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GoodCalendar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
