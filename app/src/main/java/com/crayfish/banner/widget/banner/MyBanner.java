package com.crayfish.banner.widget.banner;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by lin on 2016/8/29.
 */
public class MyBanner extends FrameLayout{

    private ViewPager adViewPager;

    public MyBanner(Context context) {
        super(context);
    }

    public MyBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
