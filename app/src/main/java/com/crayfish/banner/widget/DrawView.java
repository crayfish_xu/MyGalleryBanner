package com.crayfish.banner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by lin on 2016/8/10.
 */
public class DrawView extends View{

    private int length = 500;
    private int mCircleXY;
    private float mRadius;
    private RectF mArcRectF;
    private Paint paint;
    private Paint paintArc;
    private Paint paintText;
    private float mSweepAngle = 270;
    private String mShowText = "圆心";

    public DrawView(Context context) {
        super(context);
        init();
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        paint = new Paint();
        paint.setColor(Color.RED);

        paintArc = new Paint();
        paintArc.setStrokeWidth(20);
        paintArc.setStyle(Paint.Style.STROKE);
        paintArc.setColor(Color.BLUE);

        paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(28);

        mCircleXY = length / 2 ;
        mRadius = (float)(mCircleXY/2);
        mArcRectF = new RectF((float)( length*0.2),(float)( length*0.2),(float)( length*0.8),(float)( length*0.8));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(mCircleXY,mCircleXY,mRadius,paint);
        canvas.drawArc(mArcRectF,0,mSweepAngle,false,paintArc);
//        canvas.drawOval(mArcRectF,paintArc);
        canvas.drawText(mShowText,0,mShowText.length(),mCircleXY,mCircleXY+(mShowText.length()/2),paintText);
    }
}
