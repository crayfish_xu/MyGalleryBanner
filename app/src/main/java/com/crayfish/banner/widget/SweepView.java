package com.crayfish.banner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by lin on 2016/8/10.
 */
public class SweepView extends View{

    private Paint mPaint;
    private Matrix matrix = new Matrix();
    private float mRotate;
    private Shader mShader = null;

    public SweepView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SweepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        init();
    }

    private void init(){
        mPaint = new Paint();
        float x = 560;
        float y = 500;
        mShader = new SweepGradient(x, y, new int[] {0xFF09F68C,
                0xFFB0F44B,
                0xFFE8DD30,
                0xFFF1CA2E,
                0xFFFF902F,
                0xFF09F68C}, null);
//        mShader = new SweepGradient(x, y, new int[] { Color.CYAN,
//                Color.DKGRAY, Color.GRAY, Color.LTGRAY, Color.MAGENTA,
//                Color.GREEN, Color.TRANSPARENT, Color.BLUE }, null);
        mPaint.setShader(mShader);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        PathEffect effect = new DashPathEffect(new float[] { 5, 7}, 7);
        mPaint.setPathEffect(effect);
        mPaint.setStrokeWidth(100);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float x = 560;
        float y = 500;
        canvas.drawColor(Color.WHITE);
//        matrix.setRotate(mRotate,x,y);
//        mShader.setLocalMatrix(matrix);
//        mRotate+=10;
//        if(mRotate >= 360){
//            mRotate = 0;
//        }
//        invalidate();
        getArc(canvas,x,y,180,0,270,mPaint);
        invalidate();
    }
    public void getArc(Canvas canvas,float o_x,float o_y,float r,
                       float startangel,float endangel,Paint paint){
        RectF rectF = new RectF(o_x-r,o_y-r,o_x+r,o_y+r);
        Path path = new Path();
        path.moveTo(o_x,o_y);
        path.lineTo((float)(o_x+r*Math.cos(startangel*Math.PI/180)),
                (float)(o_y+r*Math.sin(startangel*Math.PI/180)));
        path.lineTo((float)(o_x+r*Math.cos(endangel*Math.PI/180)),
                (float)(o_y+r*Math.sin(endangel*Math.PI/180)));
        path.addArc(rectF, startangel, endangel-startangel);
        canvas.clipPath(path);
        canvas.drawCircle(o_x, o_y, r, paint);

    }
}
