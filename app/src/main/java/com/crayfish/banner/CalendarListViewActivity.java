package com.crayfish.banner;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.crayfish.banner.widget.calendar2.CalendarListView;
import com.crayfish.banner.widget.calendar2.CalendarListView2;

/**
 * Created by lin on 2016/8/23.
 */
public class CalendarListViewActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_listview);
        CalendarListView calendarListView = (CalendarListView)super.findViewById(R.id.calendarListView);
        calendarListView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
                new String[]{"A","B","C","A","B","C","A","B","C","A","B","C","A","B","C","D","E"}));
    }

}
