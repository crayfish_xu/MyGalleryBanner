package com.crayfish.banner.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by lin on 2016/9/27.
 */
public class ChooseFragmentDialog extends DialogFragment{

    public static final int SINGLE = 1;//单选
    public static final int MULTI = 2;//多选

    public ChooseFragmentDialog newInstance(int type,SelectEntity data){
        ChooseFragmentDialog cfDialog = new ChooseFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("",data);
        bundle.putInt("",type);
        cfDialog.setArguments(bundle);
        return cfDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
