package com.crayfish.banner.fragment;

import java.io.Serializable;

/**
 * Created by lin on 2016/9/27.
 */
public class SelectEntity implements Serializable{

    private int id;
    private String content;
    private boolean isSelect;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
