package com.crayfish.banner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crayfish.banner.widget.calendar2.CalendarView;
import com.crayfish.banner.widget.calendar2.WeekDayView;

/**
 * Created by lin on 2016/8/22.
 */
public class CalendarActivity2 extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar2);
        Button btn = (Button)super.findViewById(R.id.btn);
        final CalendarView calendarView  = (CalendarView)super.findViewById(R.id.calendarView);
        final WeekDayView weekDayView = (WeekDayView)super.findViewById(R.id.weekDayView);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarView.onLastMonth();
            }
        });
        calendarView.setOnDateClick(new CalendarView.DateClick() {
            @Override
            public void onDateClick(int year, int month, int day) {
                weekDayView.setDay(year, month, day);
            }
        });
        weekDayView.setOnDateClick(new WeekDayView.DateClick() {
            @Override
            public void onDateClick(int year, int month, int day) {
                calendarView.setDay(year, month, day);
            }
        });
    }
}
