package com.crayfish.banner;

import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button myViewBtn,mySweepView,calendarViewBtn;
    private Button dragView,calendarViewBtn2,calendarListView;
    private Button calendarListView2; private Button calendarListView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myViewBtn = (Button)super.findViewById(R.id.myView);
        mySweepView = (Button)super.findViewById(R.id.mySweepView);
        calendarViewBtn = (Button)super.findViewById(R.id.calendarView);
        myViewBtn.setOnClickListener(this);
        mySweepView.setOnClickListener(this);
        calendarViewBtn.setOnClickListener(this);

        dragView = (Button)super.findViewById(R.id.dragView);
        calendarViewBtn2 = (Button)super.findViewById(R.id.calendarView2);
        calendarListView = (Button)super.findViewById(R.id.calendarListView);
        dragView.setOnClickListener(this);
        calendarViewBtn2.setOnClickListener(this);
        calendarListView.setOnClickListener(this);

        calendarListView2 = (Button)super.findViewById(R.id.calendarListView2);
        calendarListView2.setOnClickListener(this);
        calendarListView3 = (Button)super.findViewById(R.id.calendarListView3);
        calendarListView3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.myView:
                startInetnt(ViewActivity.class);
                break;
            case R.id.mySweepView:
                startInetnt(SweepViewActivity.class);
                break;
            case R.id.calendarView:
                startInetnt(CalendarActivity.class);
                break;
            case R.id.dragView:
                startInetnt(ScrollerViewActivity.class);
                break;
            case R.id.calendarView2:
                startInetnt(CalendarActivity2.class);
                break;
            case R.id.calendarListView:
                startInetnt(CalendarListViewActivity.class);
                break;
            case R.id.calendarListView2:
                startInetnt(CalendarListViewActivity2.class);
                break;
            case R.id.calendarListView3:
                startInetnt(CalendarListViewActivity3.class);
                break;
        }
    }

    private void startInetnt(Class clazz){
        Intent intent = new Intent(this,clazz);
        startActivity(intent);
    }
}
